package com.ls.payoneerconverter.view;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ls.payoneerconverter.R;

/**
 * Created by Stas Safyanov on 4/23/18.
 */
public class ProgressItem extends FrameLayout {

    //Views
    private AppCompatSeekBar horizontalSeekBar;
    private TextView textThumbValue;
    private TextView tvLeftBottomInfo;
    private TextView tvRightBottomInfo;
    private ValueListener valueListener;

    public ProgressItem(Context context) {
        super(context);
        initView();
    }

    public ProgressItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ProgressItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public int getProgress() {
        return horizontalSeekBar.getProgress();
    }

    public void setProgress(int progress) {
        horizontalSeekBar.setProgress(progress);
        updateUI();
    }

    private void updateUI() {
        textThumbValue.postDelayed(new Runnable() {
            @Override
            public void run() {
                setTextThumbValue(getProgress());
            }
        }, 1);
    }

    public SeekBar getSeekBar() {
        return horizontalSeekBar;
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.custom_progressbar, null);
        addView(view);
        horizontalSeekBar = view.findViewById(R.id.seek_bar);
        textThumbValue = view.findViewById(R.id.textThumb);
        tvLeftBottomInfo = view.findViewById(R.id.tv_left_bottom_info);
        tvRightBottomInfo = view.findViewById(R.id.tv_right_bottom_info);
        horizontalSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateUI();
                if (valueListener!= null){
                    valueListener.onValueChanged(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setTextThumbValue(int progressValue) {
//        int progress = horizontalSeekBar.getProgress();
//        int max = horizontalSeekBar.getMax();
//        float offset = horizontalSeekBar.getThumbOffset();
//        float percent = ((float) progress) / (float) max;
//        float width = horizontalSeekBar.getWidth() - 2 * offset - textThumbValue.getWidth() / 2;
//        float answer = ((int) (width * percent + offset - (textThumbValue.getWidth() / 2 * percent)));
//        textThumbValue.setText(String.valueOf(progressValue));
//        textThumbValue.setX(answer);

        int progress = horizontalSeekBar.getProgress();
        int max = horizontalSeekBar.getMax();
        int offset = horizontalSeekBar.getThumbOffset();
        float percent = ((float) progress) / (float) max;
        int width = horizontalSeekBar.getWidth() - 2 * offset;

        int answer = ((int) (width * percent + offset - textThumbValue.getWidth() / 2));
        textThumbValue.setText(String.valueOf(progressValue));
        textThumbValue.setX(answer);
    }

    public void setMaxProgress(int maxProgress) {
        horizontalSeekBar.setMax(maxProgress);
    }

    public void setRightBottomText(String rightBottomText) {
        tvRightBottomInfo.setText(rightBottomText);
    }

    public void setRightBottomText(@StringRes int res) {
        tvRightBottomInfo.setText(res);
    }

    public void setLeftBottomText(String leftBottomText) {
        tvLeftBottomInfo.setText(leftBottomText);
    }

    public void setLeftBottomText(@StringRes int res) {
        tvLeftBottomInfo.setText(res);
    }

    public void setValueListener(ValueListener valueListener){
        this.valueListener = valueListener;
    }

    public interface ValueListener {

        void onValueChanged(int value);

    }

}