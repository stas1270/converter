package com.ls.payoneerconverter.presenter

import com.ls.payoneerconverter.contract.MainContract
import com.ls.payoneerconverter.utils.StringUtils

/**
 * Created by Stas Safyanov on 4/16/18.
 */
class MainPresenter(val view: MainContract.View) : MainContract.Presenter {


    init {
        getPayoneerRate()
        getBanksRate()
    }

    private fun getBanksRate() {
        //TODO("not implemented")
    }

    private fun getPayoneerRate() {
        //TODO("not implemented")
    }

    override fun clickCalculate() {

    }

    override fun calcSumFromPayoneer(sumOrCash: String, countTransaction: Int, ratePayoneer: String) {
        if (sumOrCash.isNotEmpty() && ratePayoneer.isNotEmpty()) {
            val cashSum = sumOrCash.toDouble() * countTransaction
            val rate = cashSum / ratePayoneer.toDouble()
            view.setSumFromPayoneer(StringUtils.getDoubleInFormat(rate))
            view.setSumCash(StringUtils.getDoubleInFormat(cashSum))
        }
    }

    override fun calcAvailableSumFromExchange(sumСash: String, bankRate: String) {
        if (sumСash.isNotEmpty() && bankRate.isNotEmpty()) {
            val sum = sumСash.toDouble() / bankRate.toDouble()
            view.setAvailableSumFromExchange(StringUtils.getDoubleInFormat(sum))
        }
    }

    override fun calcSumForBuy(sumForBuy: String, bankRate: String) {
        if (sumForBuy.isNotEmpty() && bankRate.isNotEmpty()) {
            val sum = sumForBuy.toDouble() * bankRate.toDouble()
            view.setBuySum(StringUtils.getDoubleInFormat(sum))
        }

    }
}