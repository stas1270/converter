package com.ls.payoneerconverter.utils

import java.text.DecimalFormat

/**
 * Created by Stas Safyanov on 4/23/18.
 */
class StringUtils {

    companion object {

        fun getDoubleInFormat(value: Double): String {
            val formatter = DecimalFormat("#0.00")
            return formatter.format(value)
        }
    }
}