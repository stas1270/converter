package com.ls.payoneerconverter.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jakewharton.rxbinding2.widget.RxTextView
import com.ls.payoneerconverter.R
import com.ls.payoneerconverter.contract.MainContract
import com.ls.payoneerconverter.presenter.MainPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by Stas Safyanov on 4/16/18.
 */

class MainActivity : AppCompatActivity(), MainContract.View {

    private lateinit var mMainPresenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initWatchersStage1()
        initWatchersStage2()
        init()
    }

    private fun init() {
        mMainPresenter = MainPresenter(this)
        f_sum_cash_from_ATM.setText("14900")
        seekBar.progress = 1
        et_exchange_rate_bank.setText("26.2")
        et_exchange_rate_payoneer.setText("25.2")
    }


    private fun initWatchersStage2() {
        RxTextView.afterTextChangeEvents(et_exchange_rate_bank)
                .skipInitialValue()
                .map { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.view().text.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    calcAvailableSumFromExchange()
                    calcSumForBuy()
                }

        RxTextView.afterTextChangeEvents(sum_cash)
                .skipInitialValue()
                .map { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.view().text.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    calcAvailableSumFromExchange()
                }

        RxTextView.afterTextChangeEvents(et_sum_exchange_need)
                .skipInitialValue()
                .map { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.view().text.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    calcSumForBuy()
                }
    }

    private fun calcSumForBuy() {
        mMainPresenter.calcSumForBuy(et_sum_exchange_need.text.toString(),
                et_exchange_rate_bank.text.toString())
    }

    private fun initWatchersStage1() {
        RxTextView.afterTextChangeEvents(f_sum_cash_from_ATM)
                .skipInitialValue()
                .map { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.view().text.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    calcSumFromPayoneer()
                }
        seekBar.setValueListener { calcSumFromPayoneer() }
        RxTextView.afterTextChangeEvents(et_exchange_rate_payoneer)
                .skipInitialValue()
                .map { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.view().text.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    calcSumFromPayoneer()
                }
    }

    private fun calcSumFromPayoneer() {
        mMainPresenter.calcSumFromPayoneer(f_sum_cash_from_ATM.text.toString(),
                seekBar.progress,
                et_exchange_rate_payoneer.text.toString())
    }

    private fun calcAvailableSumFromExchange() {
        mMainPresenter.calcAvailableSumFromExchange(sum_cash.text.toString(), et_exchange_rate_bank.text.toString())
    }

    override fun setPayoneerRate(rate: Double) {
        et_exchange_rate_payoneer.setText(rate.toString())
    }

    override fun setSumFromPayoneer(sum: String) {
        sum_of_payoneer_account.text = sum
    }

    override fun setSumCash(sum: String) {
        sum_cash.text = sum
    }

    override fun setAvailableSumFromExchange(availableSum: String) {
        sum_of_dollar_from_exchange.text = availableSum
    }

    override fun setBuySum(sumBuy: String) {
        sum_of_nat_currency_to_exchange.text = sumBuy
    }
}