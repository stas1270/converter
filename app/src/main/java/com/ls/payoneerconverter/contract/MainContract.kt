package com.ls.payoneerconverter.contract

/**
 * Created by Stas Safyanov on 4/16/18.
 */
interface MainContract {

    interface View {
        fun setPayoneerRate(rate: Double)
        fun setSumFromPayoneer(sum: String)
        fun setSumCash(sum: String)
        fun setAvailableSumFromExchange(availableSum: String)
        fun setBuySum(sumBuy: String)
    }


    interface Presenter {

        fun clickCalculate()
        fun calcSumFromPayoneer(sumOrCash: String, countTransaction: Int, ratePayoneer: String)
        fun calcAvailableSumFromExchange(sumСash: String, bankRate: String)
        fun calcSumForBuy(sumForByu: String, bankRate: String)
    }
}